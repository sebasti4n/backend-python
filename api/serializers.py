from rest_framework import serializers
from muebles.models import *

class GeneralSerializer(serializers.ModelSerializer): #Crea el Serializer para el modelo general
    class Meta:
        model = General
        fields = ('direccion','ciudad','departamento','pais','telefono')

class InteriorSerializer(serializers.ModelSerializer): #Crea el Serializer para el modelo interior
    class Meta:
        model = Interior
        fields = ('cuartos','closets','baños','calentador')

class ExteriorSerializer(serializers.ModelSerializer): #Crea el Serializer para el modelo exterior
    class Meta:
        model = Exterior
        fields = ('vigilancia','parqueadero','salon_social','numero_pisos')

class InmuebleSerializer(serializers.ModelSerializer): #Crea el Serializer para el modelo inmueble

    general = GeneralSerializer()
    interior = InteriorSerializer()
    exterior = ExteriorSerializer()

    class Meta:
        model = Inmueble
        fields = ('tipo','subtipo','general','interior','exterior') #se especifican los campos uno a uno para no imprimir id

    def create(self,validated_data): #la función create permite crear el objeto de tipo inmueble por primera vez (cuando se usa la acción POST)
        data_general = validated_data.pop('general') # almacena y elimina del diccionario los datos referentes a general
        data_interior = validated_data.pop('interior')# almacena y elimina del diccionario los datos referentes a interior
        data_exterior = validated_data.pop('exterior')# almacena y elimina del diccionario los datos referentes a exterior
        general = General.objects.create(**data_general) # los datos anteriormente almacenados se inicializan en un objeto de tipo "general"
        interior = Interior.objects.create(**data_interior)# los datos anteriormente almacenados se inicializan en un objeto de tipo "interior"
        exterior = Exterior.objects.create(**data_exterior)# los datos anteriormente almacenados se inicializan en un objeto de tipo "exterior"
        validated_data['general'] = general #se devuelven los valores de general ahora en forma de instancia de la clase "general"
        validated_data['interior'] = interior #se devuelven los valores de interior ahora en forma de instancia de la clase "interior"
        validated_data['exterior'] = exterior #se devuelven los valores de exterior ahora en forma de instancia de la clase "exterior"
        inmueble = Inmueble.objects.create(**validated_data) #se inicializa un objeto de tipo inmueble con los datos obtenidos de la acción POST con los nuevos objetos de los modelos
        return inmueble

        #El proceso de update es similar a create
    def update(self,instance,validated_data):
        data_general = validated_data.pop('general')
        data_interior = validated_data.pop('interior')
        data_exterior = validated_data.pop('exterior')
        general = General.objects.create(**data_general)
        interior = Interior.objects.create(**data_interior)
        exterior = Exterior.objects.create(**data_exterior)
        validated_data['general'] = general
        validated_data['interior'] = interior
        validated_data['exterior'] = exterior
        #Se actualiza la instancia (inmueble) con la nueva data obtenida de la acción "PUT"
        instance.tipo = validated_data.get('tipo', instance.tipo)
        instance.subtipo = validated_data.get('subtipo', instance.subtipo)
        instance.general = validated_data.get('general', instance.general)
        instance.interior = validated_data.get('interior', instance.interior)
        instance.exterior = validated_data.get('exterior', instance.exterior)
        instance.save()
        return instance
