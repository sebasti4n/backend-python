from django.shortcuts import render
from rest_framework import status #Nos muestra el estado de la acción realizada, si se crea un mueble, si no se encuentra, etc.
from rest_framework.decorators import api_view
from rest_framework.response import Response
from muebles.models import *
from .serializers import *


@api_view(['GET', 'POST']) #Decorador api_view encapsulando solo acciones GET y POST
def muebles_lista(request):
    """
    Devuelve todos los inmuebles o crea uno nuevo
    """
    if request.method == 'GET':
        mueble = Inmueble.objects.all()
        serializer = InmuebleSerializer(mueble, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = InmuebleSerializer(data=request.data)

        if serializer.is_valid(): #is_valid ejecuta tres metodos de "limpiado"(cleaning) data (valida la información)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED) #Item creado
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) #No se pudo crear


@api_view(['GET','PUT', 'DELETE'])
def muebles_detalles(request, pk):
    """
    Obtiene, actualiza o elimina un inmueble especifico
    """
    try:
        mueble = Inmueble.objects.get(pk=pk) #toma solo el inmueble almacenado en pk (argumento definido en URLS) (son las ids)
    except Inmueble.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND) #Si no existe el item
    if request.method == 'GET':
        serializer = InmuebleSerializer(mueble)
        return Response(serializer.data)
    if request.method == 'PUT':
        serializer = InmuebleSerializer(mueble, data=request.data)
        if serializer.is_valid(): #is_valid ejecuta tres metodos de "limpiado"(cleaning) data (valida la información)
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) #No se pudo actualizar
    elif request.method == 'DELETE':
        mueble.delete()
        return Response(status=status.HTTP_204_NO_CONTENT) #Elemento eliminado
