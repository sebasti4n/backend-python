from django.conf.urls import url, include
from api import views

urlpatterns=[
    url(r'^inmuebles$', views.muebles_lista, name='Lista_inmuebles'),
    url(r'^inmueble/(?P<pk>[0-9]+)$', views.muebles_detalles, name='Detalles_inmuebles'),
]
