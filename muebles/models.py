from django.db import models

class General (models.Model):
    direccion = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=50)
    departamento = models.CharField(max_length=50)
    pais = models.CharField(max_length=50)
    telefono = models.CharField(max_length=50)

class Interior (models.Model):
    cuartos = models.FloatField()
    baños = models.IntegerField()
    closets = models.IntegerField()
    calentador = models.BooleanField(null=True)

class Exterior (models.Model):
    vigilancia = models.CharField(max_length=50)
    parqueadero = models.CharField(max_length=50)
    salon_social = models.BooleanField(null=True) #no son obligatorios
    numero_pisos = models.IntegerField()

class Inmueble (models.Model):
    tipo = models.CharField(max_length=50)
    subtipo = models.CharField(max_length=50)
    general = models.ForeignKey(General, on_delete = models.CASCADE) #on_delete CASCADE: cuando el objeto referenciado es eliminado, también elimina objetos referenciados a este
    interior = models.ForeignKey(Interior, on_delete = models.CASCADE)
    exterior = models.ForeignKey(Exterior, on_delete = models.CASCADE)
