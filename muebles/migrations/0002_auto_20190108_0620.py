# Generated by Django 2.1.2 on 2019-01-08 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('muebles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exterior',
            name='salon_social',
            field=models.BooleanField(null=True),
        ),
        migrations.AlterField(
            model_name='interior',
            name='calentador',
            field=models.BooleanField(null=True),
        ),
    ]
