from django.apps import AppConfig


class MueblesConfig(AppConfig):
    name = 'muebles'
