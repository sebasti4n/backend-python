FROM python:3

RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install pipenv
RUN pipenv --python 3.7
RUN pipenv install django
