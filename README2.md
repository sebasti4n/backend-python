﻿# Inmobilio API Rest
Api rest v1, usando django y django-rest-framework y python 3.7 usando una base de datos postgres.
## Parte A

La aplicación de django **mueble** contiene los modelos de datos necesarios para la api
La validación de los datos se hace dentro de views.py usando **is_valid()** [más información.](https://docs.djangoproject.com/es/2.1/ref/forms/validation/) en la documentación de django.

## Parte B

La aplicación de django **api** contiene los serializers y el view que permite intereactuar con los modelos.


ACCIONES:
Obtener listado de todos los inmuebles:
- **GET** ~/inmuebles

Crear nuevo inmueble:
- **POST** ~/inmuebles

Obtener un inmueble específico por id:
- **GET** ~/inmueble/id

Actualizar un inmueble:
- **PUT** ~/inmueble/id

Eliminar un inmueble
- **DELETE** ~/inmueble/id

Las acciones retornan o reciben formato JSON de la siguiente forma:

		   {
		    "tipo": "string", 
		    "subtipo": "string",
		    "general": {
		      "direccion": "string",
		      "ciudad": "string",
		      "departamento": "string",
		      "pais": "string",
		      "telefono": "string"
		    },
		    "interior": {
		      "cuartos": "float",
		      "baños": "integer",
		      "closets": "integer",
		      "calentador": Boolean
		    },
		    "exterior": {
		      "vigilancia": "string",
		      "parqueadero": "string",
		      "salon_social": Boolean,
		      "numero_pisos": "integer"
		    }
		  }

Todos los datos son requeridos a excepción de los booleanos.
## STATUS:
Las respuestas frente a una acción pueden ser las siguientes:

200 - OK ("GET" satisfactorio)
201 - CREATED ("POST" satisfactorio)
204 - NOT FOUND ("DELETE" satisfactorio, elemento eliminado)
400 - BAD REQUEST (Error en "POST" o "PUT")
404 - NOT FOUND (Error al buscar un elemento "GET")

## Ejemplo

En [Este link](https://drive.google.com/file/d/1qnVqK3d11t_BWLbDFYRv6BmJ-QqOLfDK/view?usp=sharing) podrá ver la app en funcionamiento.

## Creación del Dockerfile
Se hace uso de pipenv como entorno virtual y usando el pipfile prescindimos del archivo "requeriments.txt"
	
	FROM python:3

	RUN mkdir /code
	WORKDIR /code
	COPY . /code/
	RUN pip install pipenv 
	RUN pipenv --python 3.7
	RUN pipenv install django

## Docker-Compose:
Se configura con la imagen oficial de postgreSQL para docker.

	version: '3'
	services:
	  web:
	    build: .
	    command: pipenv run python manage.py runserver 0.0.0.0:8000
	    volumes:
	      - .:/code # real time access (syncronize our container)
	    ports:
	      - "8000:8000"
	    depends_on:
	      - db
	  db:
	    image: postgres
Nota:
El docker fue construido satisfactoriamente, no obstante, no se pudieron instalar las librerías ["Geospatial"](https://docs.djangoproject.com/es/2.1/ref/contrib/gis/install/geolibs/) (GDAL,GEOS, etc..) necesarias, por ende el docker no se puede correr.
